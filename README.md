INTRODUCTION
------------

This module integrates the 'jQuery Fittext.js' plugin:
  - https://github.com/davatron5000/FitText.js,

   FitText.js, a jQuery plugin for inflating web type.

FitText makes font-sizes flexible. Use this plugin on your fluid
or responsive layout to achieve scalable headlines that fill the
parents width element.

FEATURES
--------

'jQuery FitText' plugin is:

  - Lightweight

  - Easy to use

  - Responsive

  - Customizable


REQUIREMENTS
------------

'jQuery FitText' plugin
  - https://github.com/davatron5000/FitText.js/archive/master.zip


INSTALLATION
------------

1. Download 'FitText' module archive
   - https://www.drupal.org/project/fittext

2. Extract and place it in the root modules directory i.e.
   /modules/contrib/fittext

3. Create a libraries directory in the root, if not already there i.e.
   /libraries

4. Create a fittext.js directory inside it i.e.
   /libraries/fittext.js

5. Download 'jQuery FitText' plugin
   - https://github.com/davatron5000/FitText.js/archive/master.zip

6. Place it in the /libraries/fittext.js directory i.e.
   /libraries/fittext.js/jquery.fittext.js

7. Now, enable 'FitText' module


USAGE
-----

FitText makes font-sizes flexible. Use this plugin on your responsive design
for ratio-based resizing of your headlines.

Here is a simple FitText setup:

<script>
	  $(document).ready(function() {
	    $("#responsive_headline").fitText();
	  });
</script>

Your text should now fluidly resize, by default: Font-size = 1/10th
of the element's width.


The Compressor
==============

If your text is resizing poorly, you'll want to turn tweak up/down
"The Compressor". It works a little like a guitar amp. The default is 1.

// Turn the compressor up   (resizes more aggressively).
$("#responsive_headline").fitText(1.2);

// Turn the compressor down (resizes less aggressively).
$("#responsive_headline").fitText(0.8);


minFontSize & maxFontSize
=========================

FitText now allows you to specify two optional pixel values:
minFontSize and maxFontSize.
Great for situations when you want to preserve hierarchy.

$("#responsive_headline")
  .fitText(1.2, { minFontSize: '20px', maxFontSize: '40px' });


How does it Work?
=================

1. Enable "FitText" module, Follow INSTALLATION in above.

2. Download FitText.js library, Follow INSTALLATION in above.

3. Add script to your theme/module ja file, Follow USAGE in above.

4. Enjoy that.


MAINTAINERS
-----------

Current maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt

DEMO
-----------
http://fittextjs.com/
